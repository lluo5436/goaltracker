package com.springsource.roo.exercisegoaltracker.domain;

import java.util.Date;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Activity {

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date activityDate;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "L-")
    private Date startTime;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "L-")
    private Date endTime;

    @NotNull
    private Float quantity;

    @NotNull
    @Size(min = 1)
    private String unit;

    @ManyToOne
    private Goal goal;

    private String startPlace;

    @Size(min = 5)
    private String endPlace;
}
