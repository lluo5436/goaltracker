package com.springsource.roo.exercisegoaltracker.domain;

import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

public enum GoalPriorityType {

    High, Medium, Low;

    @NotNull
    @Enumerated
    private com.springsource.roo.exercisegoaltracker.domain.GoalPriorityType priority;
}
