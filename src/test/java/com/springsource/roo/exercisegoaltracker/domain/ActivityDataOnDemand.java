package com.springsource.roo.exercisegoaltracker.domain;

import org.springframework.roo.addon.dod.RooDataOnDemand;

@RooDataOnDemand(entity = Activity.class)
public class ActivityDataOnDemand {
}
