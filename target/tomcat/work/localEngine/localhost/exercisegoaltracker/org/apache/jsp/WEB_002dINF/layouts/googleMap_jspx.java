package org.apache.jsp.WEB_002dINF.layouts;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class googleMap_jspx extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/WEB-INF/tags/util/load-scripts.tagx");
  }

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fvar_005fhtmlEscape_005fcode_005fnobody;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fcode_005farguments_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fvar_005fhtmlEscape_005fcode_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fcode_005farguments_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fvar_005fhtmlEscape_005fcode_005fnobody.release();
    _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fcode_005farguments_005fnobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
      out.write("<!DOCTYPE HTML SYSTEM \"about:legacy-compat\">\n");
      out.write("<html>");
      out.write("<head>");
      out.write("<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\"/>");
      out.write("<meta content=\"IE=8\" http-equiv=\"X-UA-Compatible\"/>");
      out.write("<meta content=\"initial-scale=1.0, user-scalable=no\" name=\"viewport\"/>");
      out.write("<style type=\"text/css\">");
      out.write("\n");
      out.write("\t\t  html { height: 100%; width: 100% }\n");
      out.write("\t\t  body { height: 100%; margin: 0px; padding: 0px }\n");
      out.write("\t\t  #map_canvas { height: 100%; margin:0px 0px 0px 280px; }\n");
      out.write("\t\t");
      out.write("</style>");
      out.write("<script src=\"https://maps.google.com/maps/api/js?sensor=true\" type=\"text/javascript\">");
      out.write("</script>");
      out.write("<script type=\"text/javascript\">");
      out.write(" \n");
      out.write("\t\t  function initialize() {\n");
      out.write("\t\t    var latlng = new google.maps.LatLng(-33.800, 151.000);\n");
      out.write("\t\t    var sydney_latlng = new google.maps.LatLng(-33.8683, 151.2086);\n");
      out.write("\t\t    var myOptions = {\n");
      out.write("\t\t      zoom: 9,\n");
      out.write("\t\t      center: latlng,\n");
      out.write("\t\t      mapTypeId: google.maps.MapTypeId.ROADMAP\n");
      out.write("\t\t    };\n");
      out.write("\t\t    var map = new google.maps.Map(document.getElementById(\"map_canvas\"),\n");
      out.write("\t\t        myOptions);\n");
      out.write("   \t\t    var marker = new google.maps.Marker({\n");
      out.write("   \t            position: map.getCenter(),\n");
      out.write("  \t            map: map,\n");
      out.write("  \t            title: 'Marker'\n");
      out.write("   \t        });\n");
      out.write("\n");
      out.write("  \t\t\t\n");
      out.write("// \t       google.maps.event.addListener(map, 'center_changed', function() {\n");
      out.write("// \t           // 3 seconds after the center of the map has changed, pan back to the\n");
      out.write("// \t           // marker.\n");
      out.write("// \t           window.setTimeout(function() {\n");
      out.write("// \t             map.panTo(marker.getPosition());\n");
      out.write("// \t           }, 3000);\n");
      out.write("//          \t});\n");
      out.write("  \t\t       \n");
      out.write("\t       google.maps.event.addListener(marker, 'click', function() {\n");
      out.write("\t    \t   map.setZoom(12);\n");
      out.write("\t    \t   map.setCenter(marker.getPosition());\n");
      out.write("\t    \t   showAlert();\n");
      out.write("\t    \t   function showAlert() {\n");
      out.write("\t    \t        window.prompt(marker.getPosition().lat()+\",\"+ marker.getPosition().lng());\n");
      out.write("\t    \t        //document.getElementById(\"_startPlace_id\").value = marker.getPosition().lat()+\",\"+ marker.getPosition().lng();\n");
      out.write("\t    \t      }\n");
      out.write("\t    \t   \n");
      out.write("\t\t         });\n");
      out.write(" \t\t       \n");
      out.write("\n");
      out.write(" \t\t      google.maps.event.addListener(map, 'click', function(event,marker,counter) {\n");
      out.write(" \t\t    \t    placeMarker(event.latLng,map,marker);\n");
      out.write(" \t\t    \t  });\n");
      out.write(" \t\t    \t}\n");
      out.write("\n");
      out.write(" \t\t    \tfunction placeMarker(location,map, marker,counter) {\n");
      out.write(" \t\t    \t\n");
      out.write("  \t\t    \t  var marker = new google.maps.Marker({\n");
      out.write("  \t\t    \t      position: location,\n");
      out.write("  \t\t    \t      map: map,\n");
      out.write("  \t\t    \t      title: 'New Marker'\n");
      out.write("  \t\t    \t  });\n");
      out.write("  \t\t\t       google.maps.event.addListener(marker, 'click', function() {\n");
      out.write("  \t\t\t    \t   map.setZoom(12);\n");
      out.write("  \t\t\t    \t   //map.setCenter(marker.getPosition());\n");
      out.write("  \t\t\t    \t   showAlert(counter);\n");
      out.write("  \t\t\t    \t   function showAlert(counter) {\n");
      out.write("  \t\t\t    \t        window.prompt(marker.getPosition().lat()+\",\"+ marker.getPosition().lng());\n");
      out.write("  \t\t\t    \t      }\n");
      out.write("  \t\t\t    \t   \n");
      out.write("  \t \t\t         });\n");
      out.write("\t\n");
      out.write(" \t\t    \t  //map.setCenter(location);\n");
      out.write(" \t\t    \t}\n");
      out.write("\n");
      out.write("\t\t  \n");
      out.write("\n");
      out.write("\t\t  ");
      out.write("</script>");
      if (_jspx_meth_util_005fload_002dscripts_005f0(_jspx_page_context))
        return;
      if (_jspx_meth_spring_005fmessage_005f0(_jspx_page_context))
        return;
      out.write("<title>");
      if (_jspx_meth_spring_005fmessage_005f1(_jspx_page_context))
        return;
      out.write("</title>");
      out.write("</head>");
      out.write("<body onload=\"initialize()\" class=\"tundra spring\">");
      out.write("<div id=\"wrapper\">");
      if (_jspx_meth_tiles_005finsertAttribute_005f0(_jspx_page_context))
        return;
      if (_jspx_meth_tiles_005finsertAttribute_005f1(_jspx_page_context))
        return;
      out.write("<div id=\"main\">");
      if (_jspx_meth_tiles_005finsertAttribute_005f2(_jspx_page_context))
        return;
      out.write("</div>");
      out.write("</div>");
      out.write("<div style=\"width:60%; height:60%\" id=\"map_canvas\"/>");
      out.write("</body>");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_util_005fload_002dscripts_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  util:load-scripts
    org.apache.jsp.tag.web.util.load_002dscripts_tagx _jspx_th_util_005fload_002dscripts_005f0 = new org.apache.jsp.tag.web.util.load_002dscripts_tagx();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_util_005fload_002dscripts_005f0);
    _jspx_th_util_005fload_002dscripts_005f0.setJspContext(_jspx_page_context);
    _jspx_th_util_005fload_002dscripts_005f0.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_util_005fload_002dscripts_005f0);
    return false;
  }

  private boolean _jspx_meth_spring_005fmessage_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  spring:message
    org.springframework.web.servlet.tags.MessageTag _jspx_th_spring_005fmessage_005f0 = (org.springframework.web.servlet.tags.MessageTag) _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fvar_005fhtmlEscape_005fcode_005fnobody.get(org.springframework.web.servlet.tags.MessageTag.class);
    _jspx_th_spring_005fmessage_005f0.setPageContext(_jspx_page_context);
    _jspx_th_spring_005fmessage_005f0.setParent(null);
    // /WEB-INF/layouts/googleMap.jspx(91,78) name = htmlEscape type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_spring_005fmessage_005f0.setHtmlEscape("false");
    // /WEB-INF/layouts/googleMap.jspx(91,78) name = var type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_spring_005fmessage_005f0.setVar("app_name");
    // /WEB-INF/layouts/googleMap.jspx(91,78) name = code type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_spring_005fmessage_005f0.setCode("application_name");
    int[] _jspx_push_body_count_spring_005fmessage_005f0 = new int[] { 0 };
    try {
      int _jspx_eval_spring_005fmessage_005f0 = _jspx_th_spring_005fmessage_005f0.doStartTag();
      if (_jspx_th_spring_005fmessage_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_spring_005fmessage_005f0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_spring_005fmessage_005f0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_spring_005fmessage_005f0.doFinally();
      _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fvar_005fhtmlEscape_005fcode_005fnobody.reuse(_jspx_th_spring_005fmessage_005f0);
    }
    return false;
  }

  private boolean _jspx_meth_spring_005fmessage_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  spring:message
    org.springframework.web.servlet.tags.MessageTag _jspx_th_spring_005fmessage_005f1 = (org.springframework.web.servlet.tags.MessageTag) _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fcode_005farguments_005fnobody.get(org.springframework.web.servlet.tags.MessageTag.class);
    _jspx_th_spring_005fmessage_005f1.setPageContext(_jspx_page_context);
    _jspx_th_spring_005fmessage_005f1.setParent(null);
    // /WEB-INF/layouts/googleMap.jspx(92,70) name = arguments type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_spring_005fmessage_005f1.setArguments((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${app_name}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    // /WEB-INF/layouts/googleMap.jspx(92,70) name = code type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_spring_005fmessage_005f1.setCode("welcome_h3");
    int[] _jspx_push_body_count_spring_005fmessage_005f1 = new int[] { 0 };
    try {
      int _jspx_eval_spring_005fmessage_005f1 = _jspx_th_spring_005fmessage_005f1.doStartTag();
      if (_jspx_th_spring_005fmessage_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_spring_005fmessage_005f1[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_spring_005fmessage_005f1.doCatch(_jspx_exception);
    } finally {
      _jspx_th_spring_005fmessage_005f1.doFinally();
      _005fjspx_005ftagPool_005fspring_005fmessage_0026_005fcode_005farguments_005fnobody.reuse(_jspx_th_spring_005fmessage_005f1);
    }
    return false;
  }

  private boolean _jspx_meth_tiles_005finsertAttribute_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  tiles:insertAttribute
    org.apache.tiles.jsp.taglib.InsertAttributeTag _jspx_th_tiles_005finsertAttribute_005f0 = new org.apache.tiles.jsp.taglib.InsertAttributeTag();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_tiles_005finsertAttribute_005f0);
    _jspx_th_tiles_005finsertAttribute_005f0.setJspContext(_jspx_page_context);
    // /WEB-INF/layouts/googleMap.jspx(97,57) name = ignore type = boolean reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_tiles_005finsertAttribute_005f0.setIgnore(true);
    // /WEB-INF/layouts/googleMap.jspx(97,57) name = name type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_tiles_005finsertAttribute_005f0.setName("header");
    _jspx_th_tiles_005finsertAttribute_005f0.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_tiles_005finsertAttribute_005f0);
    return false;
  }

  private boolean _jspx_meth_tiles_005finsertAttribute_005f1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  tiles:insertAttribute
    org.apache.tiles.jsp.taglib.InsertAttributeTag _jspx_th_tiles_005finsertAttribute_005f1 = new org.apache.tiles.jsp.taglib.InsertAttributeTag();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_tiles_005finsertAttribute_005f1);
    _jspx_th_tiles_005finsertAttribute_005f1.setJspContext(_jspx_page_context);
    // /WEB-INF/layouts/googleMap.jspx(98,55) name = ignore type = boolean reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_tiles_005finsertAttribute_005f1.setIgnore(true);
    // /WEB-INF/layouts/googleMap.jspx(98,55) name = name type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_tiles_005finsertAttribute_005f1.setName("menu");
    _jspx_th_tiles_005finsertAttribute_005f1.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_tiles_005finsertAttribute_005f1);
    return false;
  }

  private boolean _jspx_meth_tiles_005finsertAttribute_005f2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  tiles:insertAttribute
    org.apache.tiles.jsp.taglib.InsertAttributeTag _jspx_th_tiles_005finsertAttribute_005f2 = new org.apache.tiles.jsp.taglib.InsertAttributeTag();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_tiles_005finsertAttribute_005f2);
    _jspx_th_tiles_005finsertAttribute_005f2.setJspContext(_jspx_page_context);
    // /WEB-INF/layouts/googleMap.jspx(100,44) name = name type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_tiles_005finsertAttribute_005f2.setName("body");
    _jspx_th_tiles_005finsertAttribute_005f2.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_tiles_005finsertAttribute_005f2);
    return false;
  }
}
